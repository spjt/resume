#!/bin/bash

set -e

mkdir -p /source/out
cd /source/out
latex -interaction=nonstopmode ../resume.tex
dvipdfmx resume.dvi
dvipng resume.dvi
unzip ../template2.docx -d tmpword
cp resume1.png tmpword/word/media/image1.png
cp resume2.png tmpword/word/media/image2.png
(cd tmpword && zip -9r - .) > resume.docx
rm -rf tmpword


