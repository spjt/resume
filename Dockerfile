FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qq \
    && apt-get install -y \
	bash \
texlive texlive-latex-base texlive-latex-recommended texlive-latex-extra \
texlive-fonts-recommended texlive-pictures texlive-base
RUN apt-get install -y dvipng

ENV SOURCE_DIR /source
COPY compile.sh /opt/
CMD ["/opt/compile.sh"]
