#!/bin/bash

set -e

docker build -t resume-build .
docker run --rm -it -v ${PWD}:/source --user $(id -u):$(id -g) resume-build

